﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 scaleAxis;
    public float scaleUnits;

    void Update()
    {
        scaleAxis = CapsuleMovement.ClampVector3(scaleAxis);
        transform.localScale += scaleAxis * (scaleUnits * Time.deltaTime);
    }
}
